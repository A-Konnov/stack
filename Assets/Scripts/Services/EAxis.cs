﻿public enum EAxis
{
    NotAssigned = -1,
    AxisX,
    AxisZ
}
