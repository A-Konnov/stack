﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : Singleton<LevelController>
{
    private GameData _gameData;

    public Action OnFinish;

    private void Awake()
    {
        _gameData = GameData.instance;
    }

    public void StartPlay()
    {
        Score.Reset();
        PlaceStartCube();
        PlaceCube();
    }

    public void PlaceCube()
    {
        var cube = Instantiate(_gameData.cubePrefab, transform.position, Quaternion.identity, transform);
        Cubes.current = cube.GetComponent<CubeController>();
    }

    public void StopCurrentCube()
    {
        Cubes.current.move.StopMove();
        Cubes.last = CutCube.Cut();

        if (Cubes.last != null)
        {
            Score.AddScore();
            Taptic.Light();
            PlaceCube();
        }
        else
        {
            OnFinish.Invoke();
        }
    }

    private void PlaceStartCube()
    {
        var startCube = Instantiate(_gameData.startCubePrefab, transform.position, Quaternion.identity, transform);

        var newPosition = transform.position;
        newPosition.y += transform.localScale.y * 0.5f - startCube.transform.localScale.y * 0.5f;
        startCube.transform.position = newPosition;

        Cubes.last = startCube.GetComponent<CubeController>();
    }
}
