﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Score
{
    private static int _score = 0;
    public static int score => _score;

    public static int record
    {
        get
        {
            int value = PlayerPrefs.GetInt("Record");
            if (_score > value)
            {
                value = _score;
                PlayerPrefs.SetInt("Record", value);
            }

            return value;
        }
    }

    public static void AddScore()
    {
        _score++;
    }

    public static void Reset()
    {
        _score = 0;
    }
}
