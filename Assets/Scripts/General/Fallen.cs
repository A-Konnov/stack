﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fallen : Singleton<Fallen>
{
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}
