﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameData : Singleton<GameData>
{
    [Serializable]
    public struct Moving
    {
        public float step;
        public float startOffset;
        public float speed;
    }

    [SerializeField] private GameObject _cubePrefab;
    [SerializeField] private GameObject _startCubePrefab;
    [SerializeField] private Moving _moving;
    public GameObject cubePrefab => _cubePrefab;
    public GameObject startCubePrefab => _startCubePrefab;
    public Moving moving => _moving;
}