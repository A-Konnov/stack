﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CutCube
{
    private static Transform _cutCube;
    private static Transform _standingCube;

    private static Vector3 _oldPosition;
    private static Vector3 _oldScale;

    private static float _offsetX;
    private static float _offsetZ;
    
    private static float _absOffsetX;
    private static float _absOffsetZ;


    public static CubeController Cut()
    {
        _cutCube = Cubes.current.transform;
        _standingCube = Cubes.last.transform;

        _offsetX = _cutCube.position.x - _standingCube.position.x;
        _offsetZ = _cutCube.position.z - _standingCube.position.z;

        _absOffsetX = Mathf.Abs(_offsetX);
        _absOffsetZ = Mathf.Abs(_offsetZ);

        if (IsFullMissed())
        {
            Debug.Log("Fail");
            EnableGravity(_cutCube, false);
            return null;
        }

        Cutting();
        return Cubes.current;
    }

    private static bool IsFullMissed()
    {
        return _absOffsetX > _standingCube.localScale.x || _absOffsetZ > _standingCube.localScale.z;
    }

    private static void Cutting()
    {
        _oldPosition = _cutCube.position;
        _oldScale = _cutCube.localScale;

        var offsetX = _cutCube.position.x - _standingCube.position.x;
        var offsetZ = _cutCube.position.z - _standingCube.position.z;

        CreateStandingCube(offsetX, offsetZ);
        CreateFallingCube(offsetX, offsetZ);
    }

    private static void CreateStandingCube(float offsetX, float offsetZ)
    {
        var newScale = _cutCube.localScale;
        newScale.x -= Mathf.Abs(offsetX);
        newScale.z -= Mathf.Abs(offsetZ);
        _cutCube.localScale = newScale;

        var newPosition = _cutCube.position;
        newPosition.x -= offsetX * 0.5f;
        newPosition.z -= offsetZ * 0.5f;
        _cutCube.position = newPosition;

        EnableGravity(_cutCube);
    }

    private static void CreateFallingCube(float offsetX, float offsetZ)
    {
        GameObject fallCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        var absOffsetX = Mathf.Abs(offsetX);
        var absOffsetZ = Mathf.Abs(offsetZ);

        var newScale = _oldScale;
        newScale.x = absOffsetX > 0.001f ? absOffsetX : newScale.x;
        newScale.z = absOffsetZ > 0.001f ? absOffsetZ : newScale.z;
        fallCube.transform.localScale = newScale;

        var newPosition = _cutCube.position;

        if (absOffsetX > 0.001f)
        {
            if (offsetX > 0)
            {
                newPosition.x += _cutCube.localScale.x * 0.5f + absOffsetX * 0.5f;
            }
            else
            {
                newPosition.x -= _cutCube.localScale.x * 0.5f + absOffsetX * 0.5f;
            }
        }

        if (absOffsetZ > 0.001f)
        {
            if (offsetZ > 0)
            {
                newPosition.z += _cutCube.localScale.z * 0.5f + absOffsetZ * 0.5f;
            }
            else
            {
                newPosition.z -= _cutCube.localScale.z * 0.5f + absOffsetZ * 0.5f;
            }
        }

        fallCube.transform.position = newPosition;
        fallCube.AddComponent<Rigidbody>();
        fallCube.GetComponent<Renderer>().material.color = Coloring.curColor;
    }

    private static void EnableGravity(Transform cube, bool isKinematic = true)
    {
        cube.GetComponent<Collider>().isTrigger = false;
        var rb = cube.GetComponent<Rigidbody>();
        rb.useGravity = true;
        rb.isKinematic = isKinematic;
    }
}