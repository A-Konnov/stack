﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowScore : MonoBehaviour
{
    private TextMeshProUGUI _textScore;
    private int _curScore = 0;

    private void Awake()
    {
        _textScore = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        if (_curScore != Score.score)
        {
            _curScore = Score.score;
            _textScore.text = _curScore.ToString();
        }
    }
}
