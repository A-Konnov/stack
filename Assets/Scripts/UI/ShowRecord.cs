﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowRecord : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _valueTxt;

    private void Start()
    {
        _valueTxt.text = Score.record.ToString();
    }
}
