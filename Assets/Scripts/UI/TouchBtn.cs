﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchBtn : MonoBehaviour
{
    private LevelController _levelController;

    private void Awake()
    {
        _levelController = LevelController.instance;
    }

    public void OnBtn()
    {
        _levelController.StopCurrentCube();
    }
}
