﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoMove : MonoBehaviour
{
    [SerializeField] private Vector3 _startPosition;
    [SerializeField] private Vector3 _endPosition;
    [SerializeField] private float _duration;
    
    private void Start()
    {
        transform.position = _startPosition;
        
        var seq = DOTween.Sequence();
        seq.Append(transform.DOMove(_endPosition, _duration));
        seq.AppendCallback((() => { Destroy(this); }));
        seq.Play();
    }
}
