﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBCheckFinish : StateMachineBehaviour
{
    private void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        LevelController.instance.OnFinish += () => { animator.SetTrigger("Finish"); };
    }

    private void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        LevelController.instance.OnFinish -= () => { animator.SetTrigger("Finish"); };
    }
}
