﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraMove : MonoBehaviour
{
    [SerializeField] private float _duration = 1f;
    
    private float _positionY;

    private void Update()
    {
        var target = Cubes.last;
        if (target == null)
            return;
        
        if (Cubes.last.transform.position.y != _positionY)
        {
            _positionY = Cubes.last.transform.position.y;
            transform.DOMoveY(_positionY, _duration);
        }
    }
}
