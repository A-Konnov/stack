﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShowTower : MonoBehaviour
{
    private LevelController _level;
    private Camera _camera;

    private void Awake()
    {
        _level = LevelController.instance;
        _camera = Camera.main;
    }

    private void Start()
    {
        _level.OnFinish += Show;
    }

    private void Show()
    {
        var count = _level.transform.childCount;
        var newSize = _camera.orthographicSize + count * 0.2f;

        _camera.DOOrthoSize(newSize, 2f);
    }
}
