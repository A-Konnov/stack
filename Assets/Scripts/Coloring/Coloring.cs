﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coloring
{
    private static float _r;
    private static float _g;
    private static float _b;
    private static float _step = 0.1f;

    public static Color curColor => new Color(_r, _g, _b);
    
    public static Color GetRandomColor()
    {
        int color = Random.Range(0, 3);
        int addColor;
        switch (color)
        {
            case 0:
                _r = 1f;
                GetAddColors(ref _g, ref _b);
                break;
            case 1:
                _g = 1f;
                GetAddColors(ref _r, ref _b);
                break;
            case 2:
                _b = 1f;
                GetAddColors(ref _r, ref _g);
                break;
        }

        return new Color(_r, _g, _b);
    }

    public static Color GetNextColor()
    {
        Color? color = null;

        color = CheckColor(ref _r, ref _g, ref _b);
        if (color != null)
            return (Color)color;
        
        color = CheckColor(ref _g, ref _r, ref _b);
        if (color != null)
            return (Color)color;
        
        color = CheckColor(ref _b, ref _r, ref _g);
        if (color != null)
            return (Color)color;
        
        return Color.white;
    }
    
    private static void GetRandonValue(ref float color)
    {
        color = Random.Range(0f, 1f);
    }

    private static void GetAddColors(ref float color1, ref float color2)
    {
        int random = Random.Range(0, 2);
        switch (random)
        {
            case 0:
                GetRandonValue(ref color1);
                color2 = 0;
                break;
            case 1:
                GetRandonValue(ref color2);
                color1 = 0;
                break;
        }
    }

    private static Color? CheckColor(ref float baseColor, ref float color1, ref float color2)
    {
        if (baseColor >= 1f)
        {
            if (color1 <= 0 && color2 <= 0)
            {
                color1 += _step;
            }
            else if (color1 > 0 && color1 < 1)
            {
                color1 += _step;
            }
            else if (baseColor >= 1 && color1 >= 1)
            {
                baseColor -= _step;
            }
            else
            {
                color2 -= _step;
            }
            
            return new Color(_r, _g, _b);
        }

        return null;
    }
}