﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AChangeColor : MonoBehaviour
{
    private Renderer _renderer;

    protected abstract Color color { get; }

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void Start()
    {
        _renderer.material.color = color;
    }
}
