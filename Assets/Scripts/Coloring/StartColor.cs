﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartColor : AChangeColor
{
    [SerializeField] private bool _random = false;

    protected override Color color => _random ? Coloring.GetRandomColor() : Coloring.curColor;
}
