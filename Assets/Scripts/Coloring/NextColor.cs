﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextColor : AChangeColor
{
    protected override Color color => Coloring.GetNextColor();
}
