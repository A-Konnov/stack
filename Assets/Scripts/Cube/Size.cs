﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Size : MonoBehaviour
{
    void Start()
    {
        var newScale = transform.localScale;
        newScale.x = Cubes.last.transform.localScale.x;
        newScale.z = Cubes.last.transform.localScale.z;
        transform.localScale = newScale;
    }

}
