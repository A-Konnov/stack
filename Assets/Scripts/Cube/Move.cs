﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;

public class Move : MonoBehaviour
{
    private GameData _gameData;
    private Vector3 _startPosition = Vector3.zero;
    private Vector3 _endPosition = Vector3.zero;
    
    private void Awake()
    {
        _gameData = GameData.instance;
    }

    public void StartMove()
    {
        StartPosition();
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _endPosition, _gameData.moving.speed * Time.deltaTime);

        if (Vector3.SqrMagnitude(transform.position - _endPosition) < 0.001f)
        {
            var tempPos = _endPosition;
            _endPosition = _startPosition;
            _startPosition = tempPos;
        }
    }

    private void StartPosition()
    {
        var lastCube = Cubes.last;

        if (lastCube != null)
        {
            _startPosition = lastCube.transform.position;
            _startPosition.y += transform.localScale.y * 0.5f + lastCube.transform.localScale.y * 0.5f;
        }

        _endPosition = _startPosition;

        var axis = (EAxis)(Random.Range(0, 2));
        var startOffset = _gameData.moving.startOffset;
        startOffset = Random.Range(0, 2) == 0 ? startOffset : -startOffset;

        switch (axis)
        {
            case EAxis.AxisX:
                _startPosition.x += startOffset;
                _endPosition.x -= startOffset;
                break;
            case EAxis.AxisZ:
                _startPosition.z += startOffset;
                _endPosition.z -= startOffset;
                break;
        }

        transform.position = _startPosition;
    }

    public void StopMove()
    {
        Destroy(this);
    }
}
