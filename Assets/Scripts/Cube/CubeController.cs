﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{
    private GameData _gameData;
    private LevelController _level;
    private Move _move;

    public Move move
    {
        get
        {
            if (_move == null)
                _move = GetComponent<Move>();

            return _move;
        }
    }
    
    private void Awake()
    {
        _gameData = GameData.instance;
        _level = LevelController.instance;
    }
    
    private void Start()
    {
        if (move != null)
            move.StartMove();
    }
}
